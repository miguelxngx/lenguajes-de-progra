#lang racket

;;Define function deep-all-x? which receives a list containing other lists (deep list) and an element x.
;;The function returns true if every single element in the list of lists is x. Otherwise it returns false.
(define (deep-all-x? list x)
  (cond
    [(null? list) #f]
  [else (x-in-forest list x)]))

(define (leaf? tree)
  (cond
    [(empty? (rest tree)) #t]
    [else #f]))

(define (x-in-tree tree x)
  (cond
    [(leaf? tree)
     (cond
       [(= (first tree) x) #t]
       [else #f])]
    [else (and (= (first tree) x) (x-in-forest (rest tree) x))]))


(define (x-in-forest forest x)
  (cond
    [(null? forest) #t]
    [else (and (x-in-tree (first forest) x) (x-in-forest (rest forest) x))]))


;;(deep-all-x? '((1 (1 (1) (1)) (1) (1) (1))) 1) ;;#t



;;Contract deep-reverse : list of lists -> list of lists
;;Purpose: Return a list of lists with the elements reversed
;;Example: (deep-reverse '(a (b (c d)) e (f g))) should return ((g f) e ((d c) b) a)
;;Definition
(define (deep-reverse list)
  (cond
    [(null? list) '()]
    [else (deep-reverse-exe list '())]))

(define (deep-reverse-exe list res)
  (cond
    [(empty? list) res]
    [(list? (first list)) (deep-reverse-exe (rest list) (cons (reverse-list (first list) '()) res))]
    [else (deep-reverse-exe (rest list) (cons (first list) res))]))

(define (reverse-list list res)
  (cond
    [(empty? list) res]
    [(list? (first list)) (reverse-list (rest list) (cons (reverse-list (first list) '()) res))]
    [else (reverse-list (rest list) (cons (first list) res))]))

;;(deep-reverse '(a b c d))
;;(deep-reverse '((a b) (c d)))
;;(deep-reverse '(a (b ((c d) ((c d)(c d)) (c d))) e (f g)))

;;Contract flatten : list of lists -> list
;Purpose: Convert a list of list into a list respecting the order of the elements
;;Example: (flatten '(a (b (c d)) e (f g))) should produce '(a b c d e f g)
;;Definition
(define (flatten list)
  (cond
    [(empty? list) '()]
    [(list? (first list)) (append (flatten (first list)) (flatten (rest list)))]
    [else (cons (first list) (flatten (rest list)))]))


;;(flatten '(a (b (c d)) e (f g)))



;;Contract count-levels : list of lists -> number
;;Purpose: Get the max depth of a tree
;;Example: (count-levels '(a(b (c) (d))(e (f) (g))))
;;Definition
(define (count-levels list)
  (cond
    [(null? list) 0]
    [else (max_list (count-levels-exe list 1) 0)]))

(define (count-levels-exe list level)
  (cond
    [(empty? list) '()]
    [(list? (first list)) (append (count-levels-exe (first list) level) (count-levels-exe (rest list) level))]
    [else (cons level (count-levels-exe (rest list) (+ level 1)))]))

(define (max_list list max)
  (cond
    [(empty? list) max]
    [(> (first list) max) (max_list (rest list) (first list))]
    [else (max_list (rest list) max)]))
    


;;Contract count-max-arity : list of lists -> number
;;Purpose: Ger the max number of children a node has in a tree
;;Example: (count-max-arity '(a (b (c) (d))(e (f) (g) (h) (i)))) should return 4
;;Definition
(define (count-max-arity list)
  (cond
    [(empty? list) 0]
    [else (children (rest list) 0 0)]))

(define (children list acum y)
  (cond
    [(empty? list) y] 
    [(empty? (rest(first list))) (children (rest list) (+ acum 1) (compare (+ acum 1) y))]
    [(list? (rest(first list)))(compare (children (rest(first list)) 0 y)(children (rest list)(+ acum 1)(compare (+ acum 1) y)))]))


(define (compare num1 num2)
  (cond
    [(> num1 num2) num1]
    [else num2]))