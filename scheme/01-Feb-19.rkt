#lang racket
(define (leaf? tree)
  (cond
    [(empty? (cdr tree))true]
    [else false]))

(define (children tree)
  (cdr tree))

(define (count-leaves tree)
  (if (leaf? tree)
      1
      (count-leaves-in-forest (children tree))));;calls in-forest to explore the width (mutual recursion)

(define (count-leaves-in-forest forest)
  (if (null? forest)
      0
      (+ (count-leaves (car forest))
         (count-leaves-in-forest (cdr forest)))))

(leaf? '(a (b (c) (d)) (e (f) (g))))
(leaf? '(a))
(leaf? '(b (e) (d (j) (k))))
(leaf? '(b (c) (d)))
(children '(a (b (c) (d)) (e (f) (g))))
(count-leaves '(a (b (c) (d) (p)) (e (f) (g))))
(count-leaves '(a (b (c) (d) (p)) (e (f) (g))))

(define (count-values-in-forest forest)
  (if (null? forest)
      0
      (+ (count-values (car forest))
         (count-values-in-forest(cdr forest)))))

(define (count-values tree)
  (if (leaf? tree)
      (first tree)
      (+ (count-values-in-forest (children tree)) (first tree))))

(count-values '(1 (4) (3)))

;;(define (merge left right)
  ;(if ((first left)>(first right)
   ;  (append (append '() (first left)) (merge (cdr left) right))
    ; (append (append '() (first right))(merge left (cdr right))))))

(define (split-odd l)
  (cond
    [(null? l) '()]
    [(null? (cdr l))l]
    [else (cons (car l (split-odd (cdr (cdr l)))))]))

(define (split-even l)
  (cond
    [(null? l) '()]
    [(null? (cdr l)) '()]
    [else (cons (car (cdr l)) (split-even(cdr (cdr l))))]))

(define (merge l1 l2)
  (cond
    [(null? l1) l2]
    [(null? l2) l1]
    [(< (car l1) (car l2)) (cons car l1 (merge (cdr l1) l2))]
    [else (cons (car l2 (merge (cdr l2) l1)))]))

(define (merge-sort l)
  (cond
    [(null? l) l]
    [(null? (cdr l))l]
    [else
     (merge
      