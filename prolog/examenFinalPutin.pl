getHead([H|_],H).

verb(play).
verb(do).
verb(have).
verb(he,is).
verb(she,is).
verb(i,am).

subject(he).
subject(she).
subject(i).
subject(we).

property(is,mine).
property(is,my).
property(is,his).
property(is,hers).

context(play, futbol).
context(do, my).
context(my, homework).
context(have,a).
context(a,brother).
context(my,brother).

ohgod(List):-
  ohgod(List,_).

ohgod([],_):-!.


ohgod([H|T], Last):-
  subject(H),
  ohgod(T, H);

  verb(Last,H),
  ohgod(T,H);

  context(Last,H),
  ohgod(T,H);

  verb(Last,H),
  ohgod(T, H);

  subject(Last),
  verb(H),
  ohgod(T,H);

  property(Last,H),
  ohgod(T,H);

  context(Last, H),
  ohgod(T,H).

%%teste
