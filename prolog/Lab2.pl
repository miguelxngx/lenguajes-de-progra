%auxiliary
push_front(El,Li,[El|Li]).

mod(X,Y,X):-
    X < Y.

mod(X,Y,Z):-
    X1 is X-Y,
    mod(X1,Y,Z).
%is still auxiliary
push_front_list([],List2,List2).

push_front_list([H|T],List2,[H|R]):-
  push_front_list(T,List2,R).

member(X,[X|_]).

member(X,[_|T]):-member(X,T).

%go deeper still auxiliary
member_stack(E,S):-member(E,S).

getLast([H|_],H).

popFront(List,R):-
  getFrontPopped(List,[],NewList),
  invert(NewList,R).
%you dont have to comment if you have a self explanatory name.
getFrontPopped([],[_|T],T).

getFrontPopped([H|T],List,R):-
  push_front(H,List,NewList),
  getFrontPopped(T,NewList,R).

getFront(List,R):-
  findFront(List,[],R).
%not auxiliary
findFront([],[H|_],H).
%jk auxiliary
findFront([H|T],List,R):-
  push_front(H,List,NewList),
  findFront(T,NewList,R).

%not auxiliary
%yei
%any positive base case, when there's no positive, then there's no positive
any_positive([]):-not(!).

%iterate over the list finding if any value is positive.
any_positive([H|T]):-
  H>0;
  any_positive(T).

%substitute base case, when there's no element left then the head recurssion
%starts going back
substitute(_,_,[],[]).

%whenever it finds that the head of the list is equal to x it is binded
%to the result the substitute value
substitute(X,Y,[X|T],[Y|Newrest]):-
  substitute(X,Y,T,Newrest).

%when it find any other value, then it just adds the head to the
%result list
substitute(X,Y,[H|T],[H|Newrest]):-
  dif(X,H),
  substitute(X,Y,T,Newrest).

%auxiliary method to call the real one hee-hee
eliminate_duplicates(List,X):-
  eliminate_duplicates(List,[],R),
  invert(R,X).

%base case, when there is nothing in the list it binds
%the stack of duplicates to the result.
eliminate_duplicates([],X,X).

%when the head hasn't been added to the duplicate stack it adds
%the head to it.
eliminate_duplicates([H|T],X,R):-
  not(member_stack(H,X)),
  push_front(H,X,NewX),
  eliminate_duplicates(T,NewX,R).

%when the head has been added previously it just passes the stack
%as it is
eliminate_duplicates([H|T],X,R):-
  member_stack(H,X),
  eliminate_duplicates(T,X,R).

%auxiliary method to call the actual method
intersect(ListA,ListB,R):-
  intersect(ListA,ListB,[],X),
  invert(X,R),
  !.

%base case, when theres nothing in the ListA, then theres nothing
%to intersect, it doesn't check that ListB is empty since it
%stays as it is from the beggining because the head is checking
%each time if it exists on it.
intersect([],_,X,X).

%if the head is member of ListB and it hasn't been added to the Result
%stack, this makes that if an element repeats in any list, it doesn't
%add it n-times.
intersect([H|T],List,X,R):-
  member_stack(H,List),
  not(member_stack(H,X)),
  push_front(H,X,New_List),
  intersect(T,List,New_List,R),
  !.

%if the head is part of listB, but has been added it is ignored
intersect([H|T],List,X,R):-
  member_stack(H,List),
  member_stack(H,X),
  intersect(T,List,X,R),
  !.

%if the head doesn't exists in b, then is ignored.
intersect([H|T],List,X,R):-
  not(member_stack(H,List)),
  intersect(T,List,X,R),
  !.

%Base case, when it reaches the end of the list it starts going
%back(Head recurssion), and starts adding the values, but in this
%point it only returns an empty list
less_then(_,[],[]).

%when H is less than X then it adds to NewRest, This is modeled
% in a very weird way, but thrust me it works, its 2am im dying
%aaaanyway, when it isnt less, then just pass the current list
less_then(X,[H|T],Newrest):-
  H<X,
  less_then(X,T,Temp),
  push_front(H,Temp,Newrest);
  less_then(X,T,Newrest).

%*copies and pastes the comment from above and just changes less
% for equal or more*
%Base case, when it reaches the end of the list it starts going
%back(Head recurssion), and starts adding the values, but in this
%point it only returns an empty list
more_then(_,[],[]).

%when H is equal or more than X then it adds to NewRest, This is modeled
% in a very weird way, but thrust me it works, its 2am im dying
%aaaanyway, when it isnt equal or more, then just pass the current list
more_then(X,[H|T],Newrest):-
  X=<H,
  more_then(X,T,Temp),
  push_front(H,Temp,Newrest);
  more_then(X,T,Newrest).

%auxiliary to call the actual method
invert(List,Result):-
  invert(List,[],Result).

%base case, when it hits this point, then it returns the Current stack
%as the result.
invert([],Res,Res):-!.

%tail recurssion. It pushes the head to the current stack.
invert([H|T],Temp,Res):-
  push_front(H,Temp,Nose),
  invert(T,Nose,Res).

%auxiliary method that calls get Element method
getElement(List,Index,Element):-
  getElement(List,Index,0,Element).

%OMG 173 lines for a Homework

%when it reaches the element ir returns its value.
getElement([H|_],Current,Current,H).

%it iterates until it reaches the value. Head is discarded, is
%still not its turn.
getElement([_|T],Index,Current,Element):-
  dif(Index,Current),
  Current1 is Current+1,
  getElement(T,Index,Current1,Element).

%auxiliary to count the elements of a list.
count(List,X):-
  count(List,0,X).

%When it reaches the end it returns Tot wich is the quantity of elements
count([],Tot,Tot).

%if it hasn't reached end, then it continues, just adds 1 to cur
count([_|T],Cur,Tot):-
  X1 is Cur+1,
  count(T,X1,Tot).

%"construction method" for the actual method, it first counts the elements
%and then calls the method
rotate(List,Amount,X):-
  count(List,Q),
  rotate(List,Amount,0,Q,X).

%base case, when the index reaches the end, it returns an empty list
rotate(_,_,Current,Current,[]).

%Next day, Im not aware why I did it in head recurssion... aaaanyway
%it getss the element - the amount desired.
rotate(List,Amount,Current,Q,X):-
  Get is Amount + Current,
  El is Get mod Q,
  getElement(List,El,Thing),
  Current1 is Current+1,
  rotate(List,Amount,Current1,Q,Res),
  push_front(Thing,Res,X).

%todos los caminos llevan a roma
road(genua,placentia).
road(genua,pisae).
road(genua,roma).
road(placentia,ariminum).
road(pisae,roma).
road(ariminum,ancona).
road(ariminum,roma).
road(ancona,castrum_truentinum).
road(castrum_truentinum,roma).
road(capua,roma).
road(brundisium,capua).
road(messana,capua).
road(rhegium,messana).
road(lilibeum,messana).
road(catina,rhegium).
road(syracusae,catina).

%bidirectional Hee Hee
is_Road(X,Y):-
  road(X,Y);
  road(Y,X).

%haha it doesn't work, i mean yeas, but actually no. Just ignore this until
%the next comment where it works. Don't try this to get the path, the actual
%method is under this one.
path(Origin,Destiny,[Origin,Destiny]):-
  is_Road(Origin,Destiny).

path(Origin,Destiny,[Origin|Path]):-
  is_Road(Origin,Mid),
  path(Mid,Destiny,Path).
%auxiliaries.
empty_queue([]).

empty_list([]).

%I don't have words to describe from this point each part.
% this works using BFS, it manages the queue as a queue of lists
% that contains the roots. The head of each lists is the last node
%visited in the route, so it stops when it reaches the Destiny.
%If the last node is not the destiny then it looks for its neighboors.
%If the node has been visited then it is not visited again.
getPaths(_,[],R,R):-!.

getPaths(Original,[H|T],Current,R):-
  push_front(H,Original,NewC),
  push_front(NewC,Current,NewestC),
  getPaths(Original,T,NewestC,R).

visitPath(Q,Destiny,_,Current):-
  getFront(Q,Current),
  getLast(Current,H),
  H=Destiny,
  !.

visitPath(Q,Destiny,Been_list,R):-
  getFront(Q,Current),
  popFront(Q,New_Q),
  getLast(Current,H),
  member_stack(H,Been_list),
  visitPath(New_Q,Destiny,Been_list,R),
  !.

visitPath(Q,Destiny,Been_list,R):-
  getFront(Q,Current),
  popFront(Q,Aux),
  getLast(Current,H),
  not(member_stack(H,Been_list)),
  push_front(H,Been_list,New_Been_list),
  findall(X,is_Road(H,X),NewNodes),
  getPaths(Current,NewNodes,[],NewPaths),
  push_front_list(NewPaths,Aux,New_Q),
  visitPath(New_Q,Destiny,New_Been_list,R),
  !.

best_path(Start,Goal,R):-
  empty_queue(Q),
  empty_queue(T),
  push_front(Start,T,NewT),
  push_front(NewT,Q,New_Q),
  empty_list(Been_list),
  visitPath(New_Q,Goal,Been_list,Ans),
  invert(Ans,R).
