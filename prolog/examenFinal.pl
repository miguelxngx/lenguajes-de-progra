push_front(A,B, [A|B]).

member(X,[X|_]).
member(X,[_|T]):- member(X,T).

milk(goat_milk,animal).
milk(cow_milk,animal).
milk(sheep_milk,animal).
milk(wolf_milk,animal).

milk(coconut_milk,fruit).
milk(soy_milk,fruit).
milk(almond_milk,fruit).

related(cow_milk,goat_milk).
related(cow_milk,sheep_milk).
related(sheep_milk,goat_milk).

related(coconut_milk,almond_milk).
related(coconut_milk,soy_milk).
related(soy_milk,almond_milk).

stock(cow_milk,whole_milk).
stock(cow_milk,lactose_free_milk).
stock(cow_milk,flavored_milk).

stock(goat_milk,raw_milk).
stock(goat_milk,whole_milk).

stock(sheep_milk,raw_milk).
stock(sheep_milk,organic_milk).

stock(wolf_milk,raw_milk).

stock(coconut_milk,organic_milk).
stock(coconut_milk,gluten_free).

stock(soy_milk,gluten_free).
stock(soy_milk,soy_milk_free).

stock(almond_milk,organic_milk).
stock(almond_milk,flavored_milk).

is_related(X,Y):-
  dif(X,Y),
  related(X,Y);
  dif(X,Y),
  related(Y,X).


query(Milk,X):-
  milk(Milk,ComesFrom),
  push_front(ComesFrom,[],Aux),
  push_front("Comes From: ", Aux, Aux1),
  findall(S,stock(Milk,S),Stock),
  push_front(Stock, Aux1, Aux2),
  push_front("Stock: ", Aux2, Aux3),
  findall(W,is_related(Milk,W), Related),
  push_front(Related,Aux3,Aux4),
  push_front("Related milks: ",Aux4,X),!;

  milk(Milk,ComesFrom),
  push_front(ComesFrom,[],Aux),
  push_front("Comes From: ", Aux, Aux1),
  findall(S,stock(Milk,S),Stock),
  push_front(Stock, Aux1, Aux2),
  push_front("Stock: ", Aux2, Aux3),
  findall(W,is_related(Milk,W), Related),
  push_front(Related,Aux3,Aux4),
  push_front("Related milks: ",Aux4,X),!;

  milk(_,Milk),
  findall(Y,milk(Y,Milk),Res),
  push_front(Res,[],Aux),
  push_front("Milks of this type: ",Aux, X),!;

  stock(_,Milk),
  findall(Y,stock(Y,Milk),Res),
  push_front(Res,[],Aux),
  push_front("Stock Milks: ",Aux, X),!;

  push_front("The milk could not be found in the database:(", [], X),!.


%%Usage

%query(cow_milk,X). returns the information of cow_milk
%query(type_of_milk, X). returns a list of milks of that type. Types: animal, fruit
%query(type_in_stock,X). returns a list with the current milks that match the type in stock. type_in_stock: organic_milk, soy_milk_free, raw_milk
