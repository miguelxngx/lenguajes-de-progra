movie(chuck_norris,invencibles).
movie(chuck_norris,la_furia_del_dragon).
movie(jackie_chan,no_se).
movie(bruce_lee,pelicula_china_de_los_80).
movie(bruce_lee,operacion_dragon).
movie(bruce_lee,la_furia_del_dragon).
movie(jackie_chan,una_pareja_explosiva).
movie([chuck_norris|jackie_chan],rush_hour). /*declaring it with a list*/

is_movie(invencibles).
is_movie(la_furia_del_dragon).
is_movie(no_se).
is_movie(pelicula_china_de_los_80).
is_movie(operacion_dragon).
is_movie(una_pareja_explosiva).

year(invencibles,2010).
year(la_furia_del_dragon,1980).

actor_year(X,Y,Z):-
    movie(X,Z),
    year(Z,Y).

