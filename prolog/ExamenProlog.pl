member(X,[X|_]).
member(X,[_|T]):-member(X,T).
member_of(E,S):-member(E,S).

push(E,L,[E|L]).

connection(animals,arachnids).
connection(animals,birds).
connection(animals,fish).

connection(arachnids,tarantula).
connection(birds,emu).
connection(birds,barnowl).
connection(fish,tuna).

specie(tarantula,avicularia_avicularia).
specie(emu,dormaius_novaehollandiae).
specie(barnowl,tyto_alba).
specie(tuna,tunarious_tuna).

info(tarantula,are_horrible).
info(emu,are_stoopid).
info(tuna,tastes_badD).
info(tarantula,have_a_lot_of_legs).
info(barnowl,are_owl_that_live_in_barns).

find(Or,Dest,Been_list,Path,R):-
  not(member_of(Or,Been_list)),
  push(Or,Been_list,New_Been),
  connection(Or,Y),
  push(Or,Path,New_P),
  find(Y,Dest,New_Been,New_P,R),!.

find(Dest,Dest,_,R,R).

query(Animal,R):-
  find(animals,Animal,[],[],Taxi),
  specie(Animal,Spec),
  findall(X,info(Animal,X),Info),
  push(Info,[],Aux),
  push(info,Aux,Aux2),
  push(Spec,Aux2,Aux3),
  push(specie,Aux3,Aux4),
  push(Taxi,Aux4,Aux5),
  push(taxidermy,Aux5,Aux6),
  push(Animal,Aux6,R),!.

query(Specie,R):-
  not(info(Specie,_)),
  findall(X,connection(Specie,X),Animals),
  queryList(Animals,[],R).

queryList([],R,R).

queryList([H|T],Aux,R):-
  query(H,AuxR),
  push(AuxR,Aux,NewR),
  queryList(T,NewR,R).
