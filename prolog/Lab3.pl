%pivot base case, when there's nothing in the second part of the list, then
%it returns two empty lists
pivot(_,[],[],[]).

%If X is greater than X, then it puts it in the first list
pivot(H,[X|T],[X|L],G):-
  X>H,
  pivot(H,T,L,G).

%If X is equal or less than H, then X is put in the second result list
pivot(H,[X|T],L,[X|G]):-
  X=<H,
  pivot(H,T,L,G).

%auxiliar method that calls for quick sort method
quick_sort(List,R):-
  quickie(List,[],R).

%It stops iterating when the list its over and then, it returns the second parameter
%as the answer
quickie([],R,R).

%Iterative method that first calls a pivot to divide the lists, and then it makes to recursive
%calls over the two obtained lists
quickie([H|T],X,R):-
	pivot(H,T,L1,L2),
	quickie(L1,X,NewR),
  quickie(L2,[H|NewR],R),
  !.

%todos los caminos llevan a roma
road(genua,placentia).
road(genua,pisae).
road(genua,roma).
road(placentia,ariminum).
road(pisae,roma).
road(ariminum,ancona).
road(ariminum,roma).
road(ancona,castrum_truentinum).
road(castrum_truentinum,roma).
road(capua,roma).
road(brundisium,capua).
road(messana,capua).
road(rhegium,messana).
road(lilibeum,messana).
road(catina,rhegium).
road(syracusae,catina).

%bidirectional Hee Hee
is_Road(X,Y):-
  road(X,Y);
  road(Y,X).
%Auxiliar functions
member(X,[X|_]).

member(X,[_|T]):-member(X,T).

empty_stack([]).

member_stack(E,S):-member(E,S).

stack(E,S,[E|S]).

%dls, this just works as a DFS, the only difference is that it is set a
% depth limit, it can work in some cases to return the better path, but
% for doing that you must make it in an iterative way so it returns the
% first path that reaches the goal, meaning that its the closest one.
% (in a non-weighed graph).
dls(Start,Goal,Level,R):-
  empty_stack(Empty_been_list),
  stack(Start,Empty_been_list,Been_list),
  path(Start,Goal,Level,0,Been_list,R).

path(Goal,Goal,_,_,R,R).

path(State,Goal,Limit,Current,Been_list,R):-
  Current < Limit,
  is_Road(State,Next),
  not(member_stack(Next,Been_list)),
  stack(Next,Been_list,New_Been_list),
  path(Next,Goal,Limit,Current+1,New_Been_list,R),
  !.
