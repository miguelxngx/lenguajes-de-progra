hobby(juan,kaggle).
hobby(luis,hack).
hobby(elena,tennis).
hobby(midori,videogame).
hobby(simon,sail).
hobby(simon,kaggle).
hobby(laura,hack).
hobby(hans,videogame).

compatible(X,Y):-
    hobby(X,Z),
    hobby(Y,Z).
road(genua,placentia).
road(genua,pisae).
road(genua,roma).
road(placentia,ariminum).
road(pisae,roma).
road(ariminum,ancona).
road(ariminum,roma).
road(ancona,castrum_truentinum).
road(castrum_truentinum,roma).
road(capua,roma).
road(brundisium,capua).
road(messana,capua).
road(rhegium,messana).
road(lilibeum,messana).
road(catina,rhegium).
road(syracusae,catina).

can_get_to(Orig,Dest):-
    road(Orig,Dest).

can_get_to(Orig,Dest):-
    road(Orig,Mid),
    can_get_to(Mid,Dest).

size(X,Y,Z):-
    road(X,Y),
    Z is 0.

size(X,Y,Z):-
    can_get_to(X,Y),
    size(X,Mid,Z1),
    size(Mid,Y,Z2),
    Z is Z1+Z2+1.

min(A,B,C,Z):-
    A < B,
    A < C,
    Z is A;
    B < A,
    B < C,
    Z is B;
    C < A,
    C < B,
    Z is C.

mod(X,Y,X):-
    X < Y.

mod(X,Y,Z):-
    X1 is X-Y,
    mod(X1,Y,Z).

gcd(A,0,A).

gcd(A,B,Z):-
    mod(A,B,R),
    gcd(B,R,X),
    Z is X.
