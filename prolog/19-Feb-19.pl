list([1,2,3,4,5,76,83]).

prog(X,Z):-
    list(X),
    sum_elements(X,Z).

sum_elements([],0).

sum_elements([H|T],Res):-
    sum_elements(T,NewRes),
    Res is NewRes+H.

add_elements(X,Term,[X|Term]).
add_elements(X,Term,[X|Term],Y):-
    sum_elements([X|term],Y).
