brother(lee,edward).
mother(salma,lee).

age(lee,8).
age(edward,21).
age(salma,44).

younger(X,Y):-
  age(X,AgeX),
  age(Y,AgeY),
  AgeX<AgeY.

brother_of(X,Y):-
  brother(X,Y);
  brother(X,Z),
  brother(Z,Y),!.

mother_of(X,Y):-
  mother(X,Y);
  mother(X,Z),
  brother(Z,Y),!.

grandmother(X,Y):-
  mother_of(X,Z),
  mother_of(Z,Y),!.

aunt(X,Y):-
  brother_of(X,Z),
  mother(Z,Y).

prefix(X,Y,R):-
  younger(X,Y),
  R is o.

middle(X,Y,R):-
  mother_of(X,Y),
  R is ka;
  brother_of(X,Y),
  R is nii;
  aunt(X,Y),
  R is ba;
  age(Y,Age),
  Age>30,
  Age<50,
  R is ba;
  grandmother(X,Y),
  R is bä.

end(X,Y,R):-
  

make_honor(X,Y,Honor):-
  prefix(X,Y,Prefix),
  middle(X,Y,Middle),
  end(X,Y,End),
  arma(Prefix,Middle,End,Honor).
