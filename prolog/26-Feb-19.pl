b(1).
b(2).
b(3).

c(1).
c(2).
c(3).

a(X,Y):-
  b(X),
  !,
  c(Y).

%graph
mov(1,2).
mov(1,3).
mov(1,4).
mov(2,5).
mov(2,6).
mov(2,7).
mov(3,8).
mov(3,9).
mov(3,6).
mov(4,5).
mov(4,7).
mov(4,11).
mov(4,9).
mov(7,8).
mov(7,10).
mov(8,9).

%auxiliary
member(X,[X|_]).

member(X,[_|T]):-member(X,T).

empty_stack([]).

member_stack(E,S):-member(E,S).

stack(E,S,[E|S]).

%depth
go(Start,Goal,R):-
  empty_stack(Empty_been_list),
  stack(Start,Empty_been_list,Been_list),
  path(Start,Goal,Been_list,R).

path(Goal,Goal,R,R).

path(State,Goal,Been_list,R):-
  mov(State,Next),
  not(member_stack(Next,Been_list)),
  stack(Next,Been_list,New_Been_list),
  path(Next,Goal,New_Been_list,R),
  !.
