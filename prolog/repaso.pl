hobby(juan,kaggle).
hobby(luis,hack).
hobby(elena,tennis).
hobby(midori,videogame).
hobby(simon,sail).
hobby(simon,kaggle).
hobby(laura,hack).
hobby(hans,videogame).

compatible(X,Y):-
  hobby(X,Z),
  dif(X,Y),
  hobby(Y,Z).

road(genua,placentia).
road(genua,pisae).
road(genua,roma).
road(placentia,ariminum).
road(pisae,roma).
road(ariminum,ancona).
road(ariminum,roma).
road(ancona,castrum_truentinum).
road(castrum_truentinum,roma).
road(capua,roma).
road(brundisium,capua).
road(messana,capua).
road(rhegium,messana).
road(lilibeum,messana).
road(catina,rhegium).
road(syracusae,catina).

can_get_to(X,Y):-
  road(X,Y).

can_get_to(X,Y):-
  road(X,Z),
  can_get_to(Z,Y).

min(A,B,C,Z):-
  A < B,
  A < C,
  Z is A,
  !;
  B < A,
  B < C,
  Z is B,
  !;
  C < B,
  C < A,
  Z is C,
  !.

concat(A, B, [A|B]).

eliminate_duplicates([],Res,Res).

eliminate_duplicates([H|T],[H|X],Res):-
  eliminate_duplicates(T,X,Res).

eliminate_duplicates([H|T],[Y|X],Res):-
  dif(Y,H),
  concat(Y,X,Aux),
  concat(H,Aux,NewList),
  eliminate_duplicates(T,NewList,Res).
