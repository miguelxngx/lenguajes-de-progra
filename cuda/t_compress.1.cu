// template provided for cuda quizz 3.
// remember to write your own comments in the code below.

#include <stdio.h>
#define N 9		//size of original matrix
#define K N/3		//size of compressed matrrix
#define NumBlocks 			9  		// choose wisely
#define ThreadsPerBlock  	((N*N)/NumBlocks)+1	 // choose wisely

__device__ void compressing(int col, int row, float *mat, int n, float *comp, int k){
    int index = col + row * k;
    comp[index] = 0;
    int shift_row = row*3;
	int shift_col = col*3;
	//printf("%d\n",index);
    for(int m_row=shift_row; m_row<shift_row+3; m_row++){
        for(int m_col = shift_col; m_col < shift_col+3 ; m_col++){
			comp[index]+=mat[m_col+m_row*n];
			//printf("%d\n",comp[index]);
            if(index==0){
                printf("row %i col %i mat[] = %f\n", m_row, m_col, mat[col*row*n]);
            }
        }
    }
    comp[index]=comp[index]/9;
}

__global__ void compress(float *mat, int n, float *comp, int k){
	int col = threadIdx.x + blockIdx.x * blockDim.x;
	int row = threadIdx.y + blockIdx.y * blockDim.y;
	//printf("%d %d\n", col, row);
	if(k>row && k>col){
		//printf("%d %d\n", col, row);
		compressing(col, row, mat, n, comp, k);
	}
}

void print_mat(float *mat, int n){
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			printf("%f\t", mat[i*n+j]);
		}
		printf("\n");
	}
	printf("\n");
}


void fill_mat(float *mat, int n){
	int c = 0;
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			mat[i*n+j] = c++;
		}
	}
}

int main(){
	float *h_compress, *h_matrix;
	float *d_compress, *d_matrix;

	h_compress = (float *)malloc(sizeof(float)*K*K);
	h_matrix = (float *)malloc(sizeof(float)*N*N);
	
	cudaMalloc((void**)&d_compress, sizeof(float)*K*K);
	cudaMalloc((void**)&d_matrix, sizeof(float)*N*N);

	fill_mat(h_matrix, N);

	printf("\n input mat \n");
	print_mat(h_matrix, N);

	cudaMemcpy(d_matrix, h_matrix, sizeof(float)*N*N, cudaMemcpyHostToDevice);

	dim3 Blocks(K,K);
	dim3 Threads(ThreadsPerBlock,ThreadsPerBlock);

	compress<<<Blocks,Threads>>>(d_matrix,N,d_compress,K);

	cudaMemcpy(h_compress,d_compress, sizeof(float)*K*K, cudaMemcpyDeviceToHost);
	print_mat(h_compress,K);

	free(h_compress);
	free(h_matrix);
	cudaFree(d_compress);
	cudaFree(d_matrix);
}