#include <cuda_runtime.h>
#include <stdio.h>

#define N       8
#define width   10
#define TPB     512

__global__ void add(int *a, int *b, int *c, int max){
    int id=threadIdx.x + blockIdx.x * blockDim.x;
    while(id<max){
        c[id]=a[id]+b[id];
        id+= blockDim.x * gridDim.x;
    }
}

__global__ void sum(int a, int b, int *c){
    *c=a+b;
}


void fill_mat(int* M){
    for(int i=0; i<N; i++){
        for(int j=0;j<N;j++){
            M[(i*N)+j]=(i*N)+j;
        }
    }
}

int main(){
    //int *arg;
    int *a, *b, *c;
    int *d_a, *d_b, *d_c;
    int tam=N*N*sizeof(int);

    cudaMalloc((void**)&d_a, tam);
    cudaMalloc((void**)&d_b, tam);
    cudaMalloc((void**)&d_c, tam);

    a=(int*)malloc(tam);
    b=(int*)malloc(tam);
    c=(int*)malloc(tam);

    fill_mat(a);
    fill_mat(b);

    for(int i=0;i<N;i++){
        for(int j=0;j<N;j++){
            printf("%d ", a[(i*N)+j]);
        }
        printf("\n");
    }

    cudaMemcpy(d_a,a,tam,cudaMemcpyHostToDevice);
    cudaMemcpy(d_b,b,tam,cudaMemcpyHostToDevice);

    add<<<N*N,TPB>>>(d_a,d_b,d_c,N*N);

    cudaMemcpy(c,d_c,tam,cudaMemcpyDeviceToHost);

    for(int i=0;i<N;i++){
        for(int j=0;j<N;j++){
            printf("%d ", c[(i*N)+j]);
        }
        printf("\n");
    }

    long int i, acum=0;

    bool success=true;
    for (int i=0; i<(N*N); i++) {
        if ((a[i] + b[i]) != c[i]) {
            printf("Error: %d + %d != %d\n", a[i], b[i], c[i] );
            success = false;
        }
    }
    if (success) printf( "We did it!\n" );

    free(a);
    free(b);
    free(c);
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
    return 0;
}