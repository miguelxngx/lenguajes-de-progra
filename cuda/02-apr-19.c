#include "cuda_runtime.h"
#include <stdio.h>

#define N                   (4096*4096)
#define HIILOS_POR_BLOQUE   512

__gloabal__ void sumaenlagpu(int *a, int *b, int *c, int n){
    int index=threadIdx.x+blockIdx*blockDim.x;
    if(index<n){
        c[index]=a[index]+b[index];
    }
}

int main(){
    int *a, *b, *c;
    int *d_a, *d_b, d_c;
    int tam=N*sizeof(int);
    //reserva memoria en DEVICE gpu
    cudaMalloc((void**)&d_a, tam);
    cudaMalloc((void**)&d_b, tam);
    cudaMalloc((void**)&d_c, tam);

    //reserva de memoria en HOST cpu
    a = (int*)malloc(tam);
    b = (int*)malloc(tam);
    c = (int*)malloc(tam);

    //iniciamos con aleatorios
    numerosAleatorios(a,N);
    numerosAleatorios(b,N);

    //copiamos valores de cpu 'a' a gpu 'd_a'
    cudaMemcpy(d_a,a,tam,cudaMemcpyHostToDevice);
    cudaMemcpy(d_b,b,tam,cudaMemcpyHostToDevice);

    //ejecutamos la funcion clock_para tomarnos
    clock_t tiempogpu = clock();

    //ejecucion del kernel
    sumaenlagpu<<<N/HILOS_PORBLOQUE,HILOS_POR_BLEQUE>>>(d_a,d_b,d_c,N);
    printf("Tiempo transcurrido al procesador en GPU: %f\n",((double)clock()-tiempogpu)/CLOCKS_PER_SEC);

    //borramos
    free(a);
    free(b);
    free(c);
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
    return 0;
}
