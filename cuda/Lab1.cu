#include <cuda_runtime.h>
#include <stdio.h>
#include <chrono>

//prooved on a nvidia gtx 1060... 50 millionsss
#define rects 50000000
#define MAX_THREADS 65535

//Benjis function just to compare the time against thread implementation
double PI(){
    long num_rects = rects, i;
    double mid, height, width, area;
    double sum = 0.0;
    width = 1.0 / (double) num_rects;
    for (i = 0; i < num_rects; i++) {
        mid = (i + 0.5) * width;
        height = 4.0 / (1.0 + mid * mid);
        sum += height;
    }
    area = sum*width;
    return area;
}

//function that implements paralel threads
__global__ void paralelPI(double *area){
    //each thread block will calculate a set of areas
    int id=threadIdx.x + blockIdx.x * blockDim.x;
    double mid, height, width;
    width = 1.0 / (double) rects;
    //the block of threads will continue until the id surpases the number of blocrectangles
    //until that, it will save each area value in a matrix that later on will be used to sum all the values
    while(id<rects){
        mid = (id + 0.5) * width;
        height = 4.0 / (1.0 + mid * mid);
        area[id]=height;
        id+= blockDim.x * gridDim.x;
    }
}

//single thread operation that sums all the areas in a double variable.
__global__ void sumAll(double *area, double *res){
    double sum=0.0;
    double width = 1.0 / (double) rects;
    for(int i=0; i<rects; i++){
        sum+=area[i];
    }
    *res = sum * width;
}

using namespace std;
int main(){
    //main uses three main variables
    //for cpu:
    //  -area: will be used to copy the sum of all the rectangles areas calculated by the gpu
    //for gpu:
    //  -dev_area: will be used to calculate the matrix of areas of rectangles
    //  -dev_sum: will be used to store the sum of all the rectangles area
    double *area;
    double *dev_sum;
    double *dev_area;
    
    //calculates the size of memory, for each rectangle, there needs to be a double
    int tam = rects*sizeof(double);

    //memory allocate for gpu variables
    cudaMalloc((void**)&dev_area, tam);
    cudaMalloc((void**)&dev_sum, sizeof(double));

    //memory allocate for cpu variables
    area=(double*)malloc(tam);
    
    //65535 is the maximum reccomended number of thread blocks. This just calculates the number of threads that needs to be
    //calculated in each block. It adds one because if the number of rectangles is less than 65535 then, no execution is called.
    int numberOfBlocks=MAX_THREADS;
    int numberOfThreads=(rects/numberOfBlocks)+1;

    //Prints statistics
    printf("Number of blocks: %d\n", numberOfBlocks);
    printf("Number of threads per block: %d\n", numberOfThreads);
    printf("Total threads: %li\n\n", numberOfBlocks*numberOfThreads);

    //Starts timer
    auto startNoThread = chrono::steady_clock::now();
    //calls the benji function
    double areaNotConcurrent = PI();
    //Stops timer
    auto endNoThread = chrono::steady_clock::now();

    //starts timer
    auto start = chrono::steady_clock::now();
    //calls for the parallel implementation
    paralelPI<<<numberOfBlocks,numberOfThreads>>>(dev_area);
    //it doesn't stop the timer in here so it actually calculates the hole operation
    //call for the function that add all the values
    sumAll<<<1,1>>>(dev_area, dev_sum);
    //stops timer
    auto end = chrono::steady_clock::now();

    //it copy the sum of all values from gpu's variable dev_sum to cpu's variable area
    cudaMemcpy(area, dev_sum, sizeof(double), cudaMemcpyDeviceToHost);

   

    //prints the results
    printf("PI approximation: %.60lf\n", *area);
    printf("PI approximation: %.60lf\n", areaNotConcurrent);
    printf("Elapsed time in nanoseconds using threads: %llu\n", chrono::duration_cast<chrono::nanoseconds>(end - start).count());
    printf("Elapsed time in nanoseconds without threads: %llu\n", chrono::duration_cast<chrono::nanoseconds>(endNoThread - startNoThread).count());
    printf("Using threads was %llu times faster than not using threads\n", chrono::duration_cast<chrono::nanoseconds>(endNoThread - startNoThread).count()/chrono::duration_cast<chrono::nanoseconds>(end - start).count());

    //free memory
    cudaFree(dev_sum);
    cudaFree(dev_area);
    free(area);

    return 0;
}