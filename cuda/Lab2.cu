#include <cuda_runtime.h>
#include <stdio.h>
#include <chrono>
#include <math.h>

__global__ void d_matrix_mult(double *a, double *b, double *c, int n, int m){
    //printMatrix(a, 2,2);
    //shared array that is ging to store the multiplcation values in each block thread
    __shared__ double cache[255];
    //int tid = threadIdx.x + (blockDim.x*blockIdx.x);
    //the cache index is going to be the number of thread on the block
    int cacheIndex = threadIdx.x;

    //index where the elements to be multiplied are, these are calculated using the grid values for x and y as well as the x dimension
    int insertA = threadIdx.x + (blockDim.x*blockIdx.y);
    int insertB = blockIdx.x + (threadIdx.x*gridDim.x);

    //multiplication is stored in cache and waits for all the threads to do so
    cache[cacheIndex] = a[insertA] * b[insertB];
    __syncthreads();

    //reduction needs to be done using powers of 2 so we need to use the log base 2 to know wich power of 2 fits better
    //printf("%d.- %d,%d: %f * %f = %f\n",blockIdx.x+(gridDim.x*blockIdx.y), insertA, insertB, a[insertA], b[insertB], cache[cacheIndex]);
    int power=log2f(blockDim.x);
    int i=powf(2, power);
    //printf("i=%d\n", i);
    while(i!=0){
        if(cacheIndex<i){
            //printf("cache %d: %f + %f, %d\n", blockIdx.x+(gridDim.x*blockIdx.y), cache[cacheIndex], cache[cacheIndex+i], cacheIndex+i);
            cache[cacheIndex] += cache[cacheIndex+i];
        }
        __syncthreads();
        i/=2;
    }
    //thread 0 of each block will contain the sum of all the cache,
    if(cacheIndex==0){
        c[blockIdx.x+(gridDim.x*blockIdx.y)] = cache[0];
        printf("row %d * height %d col %d index %d values %.2f\n", blockIdx.y, blockDim.y, blockIdx.y, blockIdx.x+(gridDim.x*blockIdx.y), cache[cacheIndex]);
    }
}

//print matrix hee-hee
void printMatrix(double* M, int n, int m){
    int i,j;
    for(i=0; i<n; i++){
        for(j=0; j<m; j++){
            printf("%.2f ", M[j+(i*m)]);
        }
        printf("\n");
    }
}

//populate matrix for tests
void populateMatrix(double *M, int n, int m, int start){
    int i, j;
    for(i=0; i<n; i++){
        for(j=0; j<m; j++){
            M[j+(i*m)] = (j+(i*m)+start)%10;
        }
    }
}

//class that receives the matrix a and b dimensions and does all the proccess
void test(int n1, int m1, int n2, int m2){
    //matrix declaration for host and device
    double *a, *b, *c;
    double *d_a, *d_b, *d_c;
    
    //malloc for host and device
    a = (double*)malloc(sizeof(double)*n1*m1);    
    b = (double*)malloc(sizeof(double)*n2*m2);
    c = (double*)malloc(sizeof(double)*n2*m1);

    cudaMalloc((void**)&d_a, sizeof(double)*n1*m1);
    cudaMalloc((void**)&d_b, sizeof(double)*n2*m2);
    cudaMalloc((void**)&d_c, sizeof(double)*n1*m2);

    //populate dem  matreex and the print'em
    printf("Matrix a:\n");
    populateMatrix(a, n1, m1,1);
    printMatrix(a,n1,m1);
    printf("\n");
    printf("Matrix b:\n");
    populateMatrix(b, n2, m2,(n1*m1));
    printMatrix(b,n2,m2);
    printf("\n");

    //copies the data from host to device
    cudaMemcpy(d_a, a, sizeof(double)*n1*m1, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(double)*n2*m2, cudaMemcpyHostToDevice);

    //grid declaration for thread blocks, the dimensions will be the final dimensions for the product matrix(c)
    dim3 grid(m2,n1);
    //call for matrix multilication, the number of threads per block will be the number of columns in the matrix a or the rows in matrix b
    d_matrix_mult<<< grid , n2>>>(d_a, d_b, d_c, n1, m2);

    //copies the c matrix calculated by the device to the host
    cudaMemcpy(c, d_c, sizeof(double)*n1*m2, cudaMemcpyDeviceToHost);

    //prints the result
    printf("\n");
    printf("Matrix c:\n");
    printMatrix(c, n1, m2);
    printf("\n");

    //free memory
    free(a);
    free(b);
    free(c);

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
}

int main(){

    //insert the test cases hereeeee
    test(2,2,2,2);
    test(5,5,5,5);
    test(2,3,3,2);
    return 0;
}