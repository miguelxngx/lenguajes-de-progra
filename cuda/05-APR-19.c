#include<stdio.h>

int main(){
    int height=8;
    int width=10;
    int *M = (int*) malloc(sizeof(int)*height*width);
    for(int i=0; i<height; i++){
        for(int j=0;j<width;j++){
            M[(i*width)+j]=420;
        }
    }
    for(int i=0; i<height; i++){
        for(int j=0;j<width;j++){
            printf("%d ", M[(i*width)+j]);
        }
        printf("\n");
    }
    return 0;
}
