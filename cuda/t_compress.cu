// template provided for cuda quizz 3.
// remember to write your own comments in the code below.

#include <stdio.h>
#define N 9		//size of original matrix
#define K N/3		//size of compressed matrrix
#define NumBlocks 			9  		// choose wisely
#define ThreadsPerBlock  	((N*N)/NumBlocks)+1	 // choose wisely

__global__ void compress(float *mat, int n, float *comp, int k){
	__shared__ int cache[N];
	int tid = threadIdx.x + blockIdx.x*blockDim.x;
	int cacheIndex = tid;
	int i=((k*k)/2);
	int grdRow=(blockIdx.x/3);
	//printf("%d", grdRow);
	int blockRow=threadIdx.x/n;
	//printf("%d\n", (threadIdx.x%blockDim.x) + blockRow + (grdRow*n*k));
	cache[cacheIndex]=mat[(threadIdx.x%blockDim.x) + blockRow + (grdRow*n*k)];
	while(i>0){
		if(cacheIndex<i){
			cache[cacheIndex] += cache[cacheIndex+i];
		}
		i/=2;
	}
	if(cacheIndex==0){
		comp[blockIdx.x]=cache[0]/(k*k);
	}
}

void print_mat(float *mat, int n){
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			printf("%.1f\t", mat[i*n+j]);
		}
		printf("\n");
	}
	printf("\n");
}


void fill_mat(float *mat, int n){
	int c = 0;
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			mat[i*n+j] = c++;
		}
	}
}

int main(){
	float *h_compress, *h_matrix;
	float *d_compress, *d_matrix;

	h_compress = (float *)malloc(sizeof(float)*K*K);
	h_matrix = (float *)malloc(sizeof(float)*N*N);
	
	cudaMalloc((void**)&d_compress, sizeof(float)*K*K);
	cudaMalloc((void**)&d_matrix, sizeof(float)*N*N);

	fill_mat(h_matrix, N);

	printf("\n input mat \n");
	print_mat(h_matrix, N);

	cudaMemcpy(d_matrix, h_matrix, sizeof(float)*N*N, cudaMemcpyHostToDevice);

	compress<<<NumBlocks,ThreadsPerBlock>>>(d_matrix,N,d_compress,K);

	cudaMemcpy(h_compress,d_compress, sizeof(float)*K*K, cudaMemcpyDeviceToHost);
	print_mat(h_compress,K);

	free(h_compress);
	free(h_matrix);
	cudaFree(d_compress);
	cudaFree(d_matrix);
}