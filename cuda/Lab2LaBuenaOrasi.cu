#include <cuda_runtime.h>
#include <stdio.h>
#include <chrono>
#include <math.h>

__device__ void compressing(int col, int row, float *mat, int n, float *comp, int k){
    int index = col + row * k;
    comp[index] = 0;
    int shift_row = row*3;
    int shift_col = col*3;
    for(int m_row=shift_row; m_row<shift_row+3; m_row++){
        for(int m_col = shift_col; m_col; < shift_col ; m_col++){
            comp[index]+=mat[m_col+m_row*n];
            if(index==0){
                printf("row %i col %i mat[] = %f\n", m_row, m_col, mat[col*row*n];)
            }
        }
    }
    com[index]=comp[index]/9;
}




//print matrix hee-hee
void printMatrix(double* M, int n, int m){
    int i,j;
    for(i=0; i<n; i++){
        for(j=0; j<m; j++){
            printf("%.2f ", M[j+(i*m)]);
        }
        printf("\n");
    }
}

//populate matrix for tests
void populateMatrix(double *M, int n, int m, int start){
    int i, j;
    for(i=0; i<n; i++){
        for(j=0; j<m; j++){
            M[j+(i*m)] = (j+(i*m)+start)%10;
        }
    }
}

//class that receives the matrix a and b dimensions and does all the proccess
void test(int n1, int m1, int n2, int m2){
    //matrix declaration for host and device
    double *a, *b, *c;
    double *d_a, *d_b, *d_c;
    
    //malloc for host and device
    a = (double*)malloc(sizeof(double)*n1*m1);    
    b = (double*)malloc(sizeof(double)*n2*m2);
    c = (double*)malloc(sizeof(double)*n2*m1);

    cudaMalloc((void**)&d_a, sizeof(double)*n1*m1);
    cudaMalloc((void**)&d_b, sizeof(double)*n2*m2);
    cudaMalloc((void**)&d_c, sizeof(double)*n1*m2);

    //populate dem  matreex and the print'em
    printf("Matrix a:\n");
    populateMatrix(a, n1, m1,1);
    printMatrix(a,n1,m1);
    printf("\n");
    printf("Matrix b:\n");
    populateMatrix(b, n2, m2,(n1*m1));
    printMatrix(b,n2,m2);
    printf("\n");

    //copies the data from host to device
    cudaMemcpy(d_a, a, sizeof(double)*n1*m1, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(double)*n2*m2, cudaMemcpyHostToDevice);

    //grid declaration for thread blocks, the dimensions will be the final dimensions for the product matrix(c)
    dim3 grid(m2,n1);
    //call for matrix multilication, the number of threads per block will be the number of columns in the matrix a or the rows in matrix b
    d_matrix_mult<<< grid , n2>>>(d_a, d_b, d_c, n1, m2);

    //copies the c matrix calculated by the device to the host
    cudaMemcpy(c, d_c, sizeof(double)*n1*m2, cudaMemcpyDeviceToHost);

    //prints the result
    printf("\n");
    printf("Matrix c:\n");
    printMatrix(c, n1, m2);
    printf("\n");

    //free memory
    free(a);
    free(b);
    free(c);

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
}

int main(){

    //insert the test cases hereeeee
    test(2,2,2,2);
    test(5,5,5,5);
    test(2,3,3,2);
    return 0;
}